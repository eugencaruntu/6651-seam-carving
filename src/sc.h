/*
 * Seam carving using dynamic programming
 * Eugen Caruntu #29077103
 */

#ifndef SEAMCARVINGCOMP665156
#define SEAMCARVINGCOMP665156

#include <opencv2/opencv.hpp>

// the function you need to implement - by default it calls seam_carving_trivial
bool seam_carving(cv::Mat &in_image, int new_width, int new_height, cv::Mat &out_image);

cv::Mat energy_table(const cv::Mat &img);

int min_below(const cv::Mat &energy, int previous_row, int j);

std::vector<int> find_seam(const cv::Mat &energy);

bool remove_seam(const cv::Mat &iimage, cv::Mat &oimage, std::vector<int> seam);

bool seam_carving_non_trivial(const cv::Mat &in_image, int new_width, int new_height, cv::Mat &out_image);

#endif
