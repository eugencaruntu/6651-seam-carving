
#include "sc.h"

using namespace cv;
using namespace std;


/**
 * Recalculate the energy matrix. First copy row 0 from derivative.
 * Then calculate energy for the row 1 to rows count by adding the minimum of the neighbors on previous row.
 * @param energy the Mat object holding the energy values
 * @param img the image used as input
 * @return energy matrix
 */
Mat energy_table(const Mat &img) {

    Mat blured; // blur the image with a kernel of 3 to remove some noise
    GaussianBlur(img, blured, Size(3, 3), 0, 0, BORDER_DEFAULT);

    Mat gray;   // make a grayscale copy
    cvtColor(blured, gray, COLOR_BGR2GRAY);


    Mat x, y;   // Compute the Sobel derivatives on both directions
    Sobel(gray, x, CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT);
    Sobel(gray, y, CV_16S, 0, 1, 3, 1, 0, BORDER_DEFAULT);

    Mat abs_x, abs_y;   // convert to absolute values
    convertScaleAbs(x, abs_x);
    convertScaleAbs(y, abs_y);

    Mat grad;   // Add the two derivatives
    add(abs_x, abs_y, grad);
//    addWeighted(abs_x, 2, abs_y, 2, 0, grad); // we could enhance the gradient but is not always good

    // The energy matrix needs a larger type to fit values when summed-up
    Mat energy = Mat(grad.size(), CV_32S); // int

    // Set the row 0 by simply copying the values
    for (int j = 0; j < grad.cols; j++) {
        energy.at<int>(0, j) = grad.at<uchar>(0, j);
    }

    // Calculate energy from the row 1 to the rows count by adding the minimum of the neighbors in previous row
    for (int i = 1; i < grad.rows; i++) {
        for (int j = 0; j < grad.cols; j++) {
            energy.at<int>(i, j) = grad.at<uchar>(i, j) + min_below(energy, i - 1, j);
        }
    }

    return energy;
}


/**
 * Utility function to find minimum energy from neigbors on previous row. Used to build energy table from row 1
 * @param energy matrix
 * @param previous_row
 * @param j
 * @return the minimum value found
 */
int min_below(const Mat &energy, int previous_row, int j) {
    int min_val;
    // Set initially the j as the middle pixel on previous row
    min_val = energy.at<int>(previous_row, j);

    // Reduce column j if we are not already on 0 column
    if (j > 0 && energy.at<int>(previous_row, j - 1) < min_val) {
        min_val = energy.at<int>(previous_row, j - 1);
    }

    // Advance column j if we are not already on energy.cols column
    if (j < energy.cols - 1 && energy.at<int>(previous_row, j + 1) < min_val) {
        min_val = energy.at<int>(previous_row, j + 1);
    }
    return min_val;
}

/**
 * The driver function doing some sanity checks before delegating
 * @param in_image
 * @param new_width
 * @param new_height
 * @param out_image
 * @return
 */
bool seam_carving(Mat &in_image, int new_width, int new_height, Mat &out_image) {

    if (new_width > in_image.cols) {
        cout << "Invalid request!!! new_width has to be smaller than the current size!" << endl;
        return false;
    }

    if (new_height > in_image.rows) {
        cout << "Invalid request!!! ne_height has to be smaller than the current size!" << endl;
        return false;
    }

    if (new_width <= 0) {
        cout << "Invalid request!!! new_width has to be positive!" << endl;
        return false;
    }

    if (new_height <= 0) {
        cout << "Invalid request!!! new_height has to be positive!" << endl;
        return false;
    }

    return seam_carving_non_trivial(in_image, new_width, new_height, out_image);
}

/**
 * Repeatedly execute removal of seams on each direction until desired size is achieved.
 * In order to reuse code we transpose the image as necessary, but we make sure the orientation is asserted each time.
 * @param in_image
 * @param new_width
 * @param new_height
 * @param out_image
 */
bool seam_carving_non_trivial(const Mat &in_image, int new_width, int new_height, Mat &out_image) {
    Mat iimage = in_image.clone();
    Mat oimage;

    // Flags to help exit the while loop once both dimensions are satisfied
    bool height_done = false, width_done = false;

    while (!height_done || !width_done) {
        // Detect image orientation so we make a decision if we need to rotate it to reuse code
        // We do it within the while loop since orientation might change depending on target output
        bool landscape = iimage.rows < iimage.cols;

        if (iimage.rows > new_height) {
            bool rotate = (!landscape || iimage.rows < iimage.cols);
            if (rotate) {   // transpose the image if needed, to reuse code
                iimage = iimage.t();
            }
            remove_seam(iimage, oimage, find_seam(energy_table(iimage)));
            if (rotate) {   // transpose back
                oimage = oimage.t();
            }
            iimage = oimage.clone();

        } else if (iimage.rows == new_height) {
            height_done = true;
        }

        if (iimage.cols > new_width) {
            bool rotate = (landscape && iimage.rows > iimage.cols);
            if (rotate) {   // transpose the image if needed, to reuse code
                iimage = iimage.t();
            }
            remove_seam(iimage, oimage, find_seam(energy_table(iimage)));
            if (rotate) {   // transpose back
                oimage = oimage.t();
            }
            iimage = oimage.clone();

        } else if (iimage.cols == new_width) {
            width_done = true;
        }

    }   // end while

    out_image = oimage.clone();
    return true;
}

/**
 * Copy over while skipping the positions in the seam vector [v_id, entry]
 * @param iimage
 * @param oimage
 * @param seam
 */
bool remove_seam(const cv::Mat &iimage, cv::Mat &oimage, std::vector<int> seam) {
    int rows, cols;
    rows = iimage.rows;
    // Always remove a column at a time, the other case would land here transposed already so we work always on same dimension
    cols = iimage.cols - 1;
    oimage = Mat(rows, cols, CV_8UC3);

    // Copy each pixel and skip the ones in the seam to be removed
    for (int i = 0; i < rows; i++)
        for (int j = 0, k = 0; j < cols; k++, j++) {
            if (j == seam[i]) {
                k++;            // Skip the column index if marked for removal on the ith position in the seam array
            }
            oimage.at<Vec3b>(i, j) = iimage.at<Vec3b>(i, k);
        }

    return true;
}


/**
 * First finds min on (energy.rows - 1) row, then advances and finds shortest path by tracing back on min energy neigbors.
 * Makes vector of rows size: the position in array represents one dimension (i), the value represents the other (j)
 * @param energy matrix
 * @return seam the vector of column indices to be removed
 */
std::vector<int> find_seam(const cv::Mat &energy) {

    vector<int> seam(energy.rows);

    // Find min value on (energy.rows - 1) row and retain its index
    int min_val = INT_MAX;
    int min_j = 0;
    for (int j = 0; j < energy.cols; j++) {

        if (energy.at<int>(energy.rows - 1, j) < min_val) {
            min_val = energy.at<int>(energy.rows - 1, j);
            min_j = j;
        }
    }
    seam[energy.rows - 1] = min_j;

    // Advance row by row and find shortest path, keep adding to seam
    for (int i = energy.rows - 2; i >= 0; i--) {

        // Set initially the min_j as the middle pixel on previous row
        min_val = energy.at<int>(i, min_j);

        // Reduce column j if we are not already on 0 column
        if (min_j > 0 && energy.at<int>(i, min_j - 1) < min_val) {
            min_val = energy.at<int>(i, min_j - 1);
            min_j = min_j - 1;
        }

        // Advance column j if we are not already on energy.cols column
        if (min_j < energy.cols - 1 && energy.at<int>(i, min_j + 1) < min_val) {
            min_j = min_j + 1;
        }

        // record the column index on the ith position
        seam[i] = min_j;
    }

    return seam;
}
